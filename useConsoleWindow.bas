Attribute VB_Name = "useConsoleWindow"
Option Explicit

Public Const ERROR_SUCCESS As Long = 0
Public Const ERROR_HANDLE_EOF As Long = 38

Public Declare Sub Sleep Lib "kernel32" _
                            (ByVal dwMilliseconds As Long)
Public Declare Sub ExitProcess Lib "kernel32" _
                            (ByVal uExitCode As Long)

Public hWindow As Long
Public hStdIn As Long
Public hStdOut As Long
Public hStdErr As Long



Private Const STD_INPUT_HANDLE As Long = -10
Private Const STD_OUTPUT_HANDLE As Long = -11
Private Const STD_ERROR_HANDLE As Long = -12

Private Declare Function GetConsoleWindow Lib "kernel32" () As Long
Private Declare Function FreeConsole Lib "kernel32" () As Boolean
Private Declare Function AllocConsole Lib "kernel32" () As Boolean

Private Declare Function GetStdHandle Lib "kernel32" (ByVal HandleType As Long) As Long

Private Type COORD
        x As Integer
        y As Integer
End Type

Private Type SMALL_RECT
        Left As Integer
        Top As Integer
        Right As Integer
        Bottom As Integer
End Type

Private Type CONSOLE_SCREEN_BUFFER_INFO
        dwSize As COORD
        dwCursorPosition As COORD
        wAttributes As Integer
        srWindow As SMALL_RECT
        dwMaximumWindowSize As COORD
End Type

Private Declare Function GetConsoleScreenBufferInfo Lib "kernel32.dll" (ByVal hConsoleOutput As Long, _
                                                                       ByRef lpConsoleScreenBufferInfo As CONSOLE_SCREEN_BUFFER_INFO) As Integer
Private Declare Function SetConsoleTextAttribute Lib "kernel32" (ByVal hConsoleOutput As Long, ByVal wAttributes As Integer) As Long

      Public Enum Foreground
         Blue = &H1
         Green = &H2
         Red = &H4
         Intensity = &H8
      End Enum

      Public Enum Background
         Blue = &H10
         Green = &H20
         Red = &H40
         Intensity = &H80
      End Enum

Private Declare Function WriteFile Lib "kernel32" _
       (ByVal hFile As Long, _
        ByVal lpBuffer As String, _
        ByVal cToWrite As Long, _
        ByRef cWritten As Long, _
        Optional ByVal lpOverlapped As Long) As Long

Private Declare Function ReadFile Lib "kernel32" _
       (ByVal hFile As Long, _
        ByVal lpBuffer As String, _
        ByVal cToRead As Long, _
        ByRef cRead As Long, _
        Optional ByVal lpOverlapped As Long) As Long
        
Public Sub SetHandles()
    hStdIn = GetStdHandle(STD_INPUT_HANDLE)
    hStdOut = GetStdHandle(STD_OUTPUT_HANDLE)
    hStdErr = GetStdHandle(STD_ERROR_HANDLE)
    hWindow = GetConsoleWindow
End Sub

Public Function ConWrite(ByVal Handle As Long, _
                         ByVal Text As String, _
                         ByRef BytesWritten As Long) As Long
    'Returns:  ERROR_SUCCESS (= 0).
    '          Other system errors.
    If WriteFile(Handle, ByVal Text, Len(Text), BytesWritten) <> 0 Then
        ConWrite = ERROR_SUCCESS
    Else
        ConWrite = Err.LastDllError
    End If
End Function

Public Function ConWriteLine(ByVal Handle As Long, _
                             ByVal Text As String, _
                             ByRef BytesWritten As Long) As Long
    'Returns:  See ConWrite().
    Dim lngResult As Long
    Dim lngWritten As Long
    
    lngResult = ConWrite(Handle, Text, BytesWritten)
    If lngResult <> ERROR_SUCCESS Then
        ConWriteLine = lngResult
    Else
        lngResult = ConWrite(Handle, vbNewLine, lngWritten)
        If lngResult <> ERROR_SUCCESS Then
            ConWriteLine = lngResult
        Else
            BytesWritten = BytesWritten + lngWritten
        End If
    End If
End Function

Public Function ConRead(ByRef Buffer As String, _
                        ByVal Bytes As Long) As Long
    'Reads until Bytes bytes, write from other end completes, or
    'an EOF or error is encountered.
    '
    'Returns:  ERROR_SUCCESS (= 0) - Data returned (in Buffer).
    '          ERROR_HANDLE_EOF    - EOF.
    '          Other system errors.
    Dim lngRead As Long
    
    Buffer = Space$(Bytes)
    If ReadFile(hStdIn, ByVal Buffer, Bytes, lngRead) <> 0 Then
        If lngRead > 0 Then
            Buffer = Left$(Buffer, lngRead)
            ConRead = ERROR_SUCCESS
        Else
            ConRead = ERROR_HANDLE_EOF
        End If
    Else
        Buffer = ""
        ConRead = Err.LastDllError
    End If
End Function

Public Function ConReadLine(ByRef Buffer As String, _
                            ByVal Bytes As Long) As Long
    'Reads until a NewLine is found.  Discards anything
    'after the NewLine.
    '
    'Returns:  See ConRead().
    Dim intEnd As Integer
    Dim lngResult As Long
    Dim strFrag As String
    
    Buffer = ""
    Do
        lngResult = ConRead(strFrag, Bytes)
        If lngResult = 0 Then
            Buffer = Buffer & strFrag
        Else
            ConReadLine = lngResult
            Exit Function
        End If
        intEnd = InStrRev(Buffer, vbNewLine)
        If intEnd = 0 Then
            Sleep 10
        End If
    Loop Until intEnd > 0
    Buffer = Left$(Buffer, intEnd - 1)
    ConReadLine = ERROR_SUCCESS
End Function
                             
                             


Sub auto_open()
 
 
  Dim lngWritten As Long
  Dim strWord As String
  
  Dim vArgs As Variant
  vArgs = commandLine.ParseArgs("/")
  
  If vArgs(0, 0) <> False Then
    Debug.Print vArgs(0, 1)
  End If
    
  
  Debug.Print AllocConsole 'Create a console instance
  
  SetHandles
  
  ConWriteLine hStdOut, "Hello! ", lngWritten
  ConWriteLine hStdOut, "What's the word? (Type it now)", lngWritten
  ConReadLine strWord, 80
  
  Call SetConsoleTextAttribute(hStdOut, Foreground.Blue + Background.Red + Foreground.Intensity)
  ConWriteLine hStdOut, "So, the word is " & strWord, lngWritten
  
  ConReadLine strWord, 80
  Debug.Print FreeConsole
  
  
End Sub


