Attribute VB_Name = "unittest"
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Option Explicit

Private m_testRunner As ITestRunner
Private m_testReporter As IReporter

Public Enum eReporterType
    toImmediate
    toFile
    toSheet
End Enum
    
Public Enum enumReporters
    Dot
End Enum

Public Sub test(Optional ByVal bookname As String = "", Optional ByVal reporterType As eReporterType = eReporterType.toImmediate, Optional ByRef reportObjects As Variant)


    Dim reporter As IReporter
    
    Select Case reporterType
        Case eReporterType.toFile
            Set reporter = defaults.reportToFile(reportObjects)
        Case eReporterType.toSheet
            
            On Error Resume Next
            Set reporter = defaults.reportToSheet(ActiveWorkbook.Worksheets(reportObjects))
            If Err.Number <> 0 Then
                Err.Raise 0, , "the reportObject must be the name of a sheet"
            End If
            On Error GoTo 0
            
    End Select

    Call tester(reporter)
    If bookname = "" Then
        Call m_testRunner.run
    Else
        Call m_testRunner.runTestOnBook(bookname)
    End If

End Sub

Public Function tester(Optional ByRef testReporter As IReporter) As ITestRunner
    
    
    Set m_testReporter = testReporter
    
    If testReporter Is Nothing Then
        Set m_testReporter = defaults.reportToDebug
    End If
    
    Set m_testRunner = New cTester
    Call m_testRunner.setReporter(m_testReporter)
    
    Set tester = m_testRunner
    
End Function

