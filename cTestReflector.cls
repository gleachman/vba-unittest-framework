VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cTestReflector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Option Explicit
'Uses Microsoft Visual Basic for Application Extensibility
'Uses Microsoft Scripting Runtime
'Must enable programatic access to modules (Developer TAB -> Macro Security BUTTON)

Public dicOfTestClasses As Dictionary
Private noOfTests As Long
Private amod As VBComponent
Private oProj As VBProject

Public Function testableClasses() As Variant
    testableClasses = dicOfTestClasses.Keys
End Function

Public Sub createTestsClassGetters()
    Dim vClassName As Variant
    Dim sFunc As String
    Dim vComp As VBComponent
    
    If dicOfTestClasses.count > 0 Then
        For Each vComp In oProj.VBComponents
            If vComp.Name = "unittestFramework_factory" Then
                Set amod = vComp
            End If
        Next vComp
        If amod Is Nothing Then
            Set amod = oProj.VBComponents.Add(vbext_ct_StdModule)
            amod.Name = "unittestFramework_factory"
        Else
            Call amod.CodeModule.DeleteLines(1, amod.CodeModule.CountOfLines)
        End If
    
        sFunc = sFunc & "Public Function unittestFactory(byval className as string)" & vbCr
        sFunc = sFunc & "    Select case className" & vbCr
        
        For Each vClassName In testableClasses
            
            sFunc = sFunc & "        case """ & vClassName & """: set unittestFactory = new " & vClassName & vbCr
            
        Next vClassName
        sFunc = sFunc & "    end Select" & vbCr
        sFunc = sFunc & "End Function" & vbCr
        
        Call amod.CodeModule.AddFromString(sFunc)
        
    End If

    
End Sub

Public Sub removeTestsClassGetters()
    
    If Not amod Is Nothing Then
        amod.Activate
        Call oProj.VBComponents.Remove(amod)
    End If
    
End Sub

Public Sub extractTestMethodsFromTestBook(ByVal testBook As Workbook)


    Dim aComp As VBComponent
    
    Set oProj = testBook.VBProject
    Set dicOfTestClasses = New Dictionary
        
    For Each aComp In oProj.VBComponents
        Call findTestMethodsInComponent(aComp)
    Next aComp
   
    
End Sub


Public Sub findTestMethodsInComponent(ByRef comp As VBComponent)
    Dim startline As Long, startcol As Long, endline As Long, endcol As Long
    Dim foundline As Long
    Dim found As Boolean
    Dim procName As String
    Dim procs As String
    
    Dim isTestable As Boolean
    
    
    isTestable = comp.CodeModule.Find("implements ITestable", startline, startcol, endline, endcol, False, False)
    If Not isTestable Then
        Exit Sub
    End If
    startline = startline + 1
    found = comp.CodeModule.Find("Public Sub", startline, startcol, endline, endcol, False, False)
    foundline = startline
    While found
    
        
        procName = comp.CodeModule.ProcOfLine(startline, vbext_pk_Proc)
        
        If Len(procs) = 0 Then
            procs = procName
        Else
            procs = procs & ";" & procName
        End If
        
        startline = startline + 1
        found = comp.CodeModule.Find("Public Sub ", startline, startcol, endline, endcol, False, False)
        
        If foundline = startline Then
            'we cycled all the way around
            
            If procs <> "" Then
                dicOfTestClasses(comp.Name) = Split(procs, ";")
            End If
            Exit Sub
        End If
        
    Wend

End Sub
