VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "zzSutTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

'SUT Template Start
Implements ITestAble
Option Explicit

Public t As ITestRunner
' Private WithEvents sut As sutClass
' Private sut As sutClass

Private Sub ITestAble_setTestRunner(testRunner As unittestFramework.ITestRunner)
    Set t = testRunner
End Sub

Private Sub ITestAble_setUp()
    'your setup code for each test
End Sub

Private Sub ITestAble_tearDown()
    'your tear down code for each test
End Sub
'SUT Template End

Public Sub zztest_failingTest()
    
    t.AssertTrue False
    
End Sub


