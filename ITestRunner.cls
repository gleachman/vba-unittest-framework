VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ITestRunner"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Public Sub setReporter(ByRef aReporter As IReporter)
End Sub

Public Function AssertTrue(ByVal val As Boolean, Optional ByVal msg As String)
End Function

Public Function AssertEqual(ByVal lIs As Variant, ByVal lshouldbe As Variant, Optional msg As String)
End Function
Public Function AssertArrayEqual(ByVal lIs As Variant, ByVal lshouldbe As Variant, Optional msg As String)
End Function
Public Sub runTest(ByRef testClassCaller As Object, ByVal testMethod As String)
End Sub
Public Sub runTests(ByRef testClassCaller As Object, ByVal arrayOfTestNames As Variant)
End Sub
Public Sub runTestOnBook(ByVal bookname As String)
End Sub
Public Sub run()
End Sub
