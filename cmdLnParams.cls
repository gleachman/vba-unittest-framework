VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cmdLnParams"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Option Explicit

Private m_out As String
Private m_sheet As String
Private m_quit As Boolean
Private m_cmdLine As String
'

Private Sub Class_Initialize()

    m_cmdLine = CmdToStr(GetCommandLine)
    Call ParseArgs("/")


End Sub

Public Function reporter() As IReporter

    Dim aRep As IReporter
    
    If m_out <> "" Then
        Set aRep = defaults.reportToFile(m_out)
    End If
    
    Set reporter = aRep
End Function
Public Function doQuit() As Boolean
    Dim wb As Workbook
    If m_quit Then
        Application.DisplayAlerts = False
        For Each wb In Application.Workbooks
            wb.Saved = True
        Next wb
        Application.quit
    End If
End Function
Private Function CmdToStr(Cmd As Long) As String

    Dim Buffer() As Byte
    Dim StrLen As Long
    
    If Cmd Then
       StrLen = lstrlenW(Cmd) * 2
       If StrLen Then
          ReDim Buffer(0 To (StrLen - 1)) As Byte
          CopyMemory Buffer(0), ByVal Cmd, StrLen
          CmdToStr = Buffer
       End If
    End If
    
End Function

Private Function ParseArgs(ByVal seperator As String)
    
    
    Dim iStart As Integer
    Dim sArgs As String
    Dim vArgs As Variant
    Dim vArgPair As Variant
    Dim x As Integer
    Dim lUpperLimit As Long
    
    iStart = InStr(1, m_cmdLine, " /e")
    If iStart = 0 Then Exit Function
    sArgs = Mid$(m_cmdLine, iStart + 4)
    If Len(sArgs) = 0 Then Exit Function
    If InStr(sArgs, seperator) = 0 Then Exit Function
    
    vArgs = Split(sArgs, " ")
    
    lUpperLimit = UBound(vArgs)
    
    For x = 1 To lUpperLimit
        If vArgs(x) <> "" Then
            vArgPair = Split(vArgs(x), ":", 2)
            
            Select Case LCase(vArgPair(0))
                Case "/out":
                    m_out = vArgPair(1)
                Case "/quit":
                    m_quit = True
                Case "/q":
                    m_quit = True
            End Select
        End If
    Next x
    
End Function


