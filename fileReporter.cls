VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "fileReporter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Implements IReporter

Private m_fullPathFile As String
Private m_ofs As New FileSystemObject
Private mClassName As String
Private mTestName As String

Public Sub init(ByVal fullPathFile As String)
    m_fullPathFile = fullPathFile
    Call m_ofs.OpenTextFile(m_fullPathFile, ForWriting, True)
End Sub

Private Sub IReporter_onBeforeClassTests()
    Call m_ofs.OpenTextFile(m_fullPathFile, ForAppending, True).WriteLine(mClassName)
End Sub

Private Sub IReporter_onBeforeTest()

End Sub

Private Sub IReporter_onClassTestsEnd()
    Call m_ofs.OpenTextFile(m_fullPathFile, ForAppending, True).WriteLine("----------")
End Sub

Private Sub IReporter_onTestEnd()

End Sub

Private Sub IReporter_report(ByVal hasPassed As Boolean, ByVal msg As String, ParamArray args() As Variant)
    Dim lMsg As String
    lMsg = IIf(hasPassed, "PASSED", "FAILED") & " - " & mClassName & "." & mTestName & " " & IIf(hasPassed, "", msg)
    Call m_ofs.OpenTextFile(m_fullPathFile, ForAppending, True).WriteLine(lMsg)
End Sub

Private Sub IReporter_reportSummary(ByVal count As Long, ByVal ignored As Long, ByVal success As Long)
    Dim lMsg As String
    Dim resSoFar As String
    If count = 0 Then Exit Sub
    resSoFar = m_ofs.OpenTextFile(m_fullPathFile, ForReading).ReadAll
    lMsg = vbCr & vbCr & "--------------------------" & vbCr & _
           "Total Tests: " & count & vbCr & _
           "Total Passed: " & success & vbCr & _
           "Total Ignored: " & ignored & vbCr & vbCr & _
           vbCr & "--------------------------" & vbCr & _
           resSoFar
    
    Call m_ofs.OpenTextFile(m_fullPathFile, ForWriting, True).WriteLine(lMsg)
    
End Sub

Private Sub IReporter_setClass(ByVal className As String)
    mClassName = className
End Sub

Private Sub IReporter_setTest(ByVal testName As String)
    mTestName = testName
End Sub

Private Function IReporter_theTest() As String

End Function
