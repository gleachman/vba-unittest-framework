Attribute VB_Name = "templates"
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Option Explicit

Const templateTestClassString As String = _
"Implements ITestAble" & vbCr & _
"Option Explicit" & vbCr & _
"Public t As ITestRunner" & vbCr & _
"' Private sut As yourclass" & vbCr & _
"" & vbCr & _
"Private Sub ITestAble_setTestRunner(testRunner As unittestFramework.ITestRunner)" & vbCr & _
"    Set t = testRunner" & vbCr & _
"End Sub" & vbCr & _
"" & vbCr & _
"Private Sub ITestAble_setUp()" & vbCr & _
"    'your setup code for each test" & vbCr & _
"    'set sut = new yourclass" & vbCr & _
"End Sub" & vbCr & _
"" & vbCr & _
"Private Sub ITestAble_tearDown()" & vbCr & _
"    'your tear down code for each test" & vbCr & _
"End Sub" & vbCr & _
"'SUT Template End" & vbCr & _
"" & vbCr & _
"Public Sub shouldFail()" & vbCr & _
"    t.AssertTrue False" & vbCr & _
"End Sub"

Const templateBespokeReporterString As String = _
"Option Explicit" & vbCr & "Implements IReporter" & vbCr & vbCr & _
"Private cn As String" & vbCr & "Private tn As String" & vbCr & "'" & vbCr & vbCr & _
"Private Sub IReporter_onBeforeClassTests()" & vbCr & "End Sub" & vbCr & _
"Private Sub IReporter_onBeforeTest()" & vbCr & "End Sub" & vbCr & _
"Private Sub IReporter_onClassTestsEnd()" & vbCr & "End Sub" & vbCr & _
"Private Sub IReporter_onTestEnd()" & vbCr & "End Sub" & vbCr & _
"Private Sub IReporter_report(ByVal hasPassed As Boolean, ByVal msg As String, ParamArray args() As Variant)" & vbCr & _
"   Debug.Print hasPassed, cn & ""."" & tn & "" > "" & msg""" & vbCr & _
"End Sub" & vbCr & _
"Public Sub IReporter_reportSummary(ByVal count As Long, ByVal ignored As Long, ByVal success As Long)" & vbCr & "End Sub" & vbCr & _
"Private Sub IReporter_setClass(ByVal className As String)" & vbCr & _
"    cn = className" & vbCr & _
"End Sub" & vbCr & _
"Private Sub IReporter_setTest(ByVal testName As String)" & vbCr & _
"    tn = testName" & vbCr & _
"End Sub" & vbCr & _
"Private Function IReporter_theTest() As String" & vbCr & "End Function"

Public Sub BespokeReporter()
    Call ActiveWorkbook.VBProject.VBComponents.Add(vbext_ct_ClassModule).CodeModule.AddFromString(templateBespokeReporterString)
End Sub
Public Sub TestClass()
    Call ActiveWorkbook.VBProject.VBComponents.Add(vbext_ct_ClassModule).CodeModule.AddFromString(templateTestClassString)
End Sub
