VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IReporter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Public Sub setClass(ByVal className As String)

End Sub
Public Sub setTest(ByVal testName As String)

End Sub

Public Sub report(ByVal hasPassed As Boolean, ByVal msg As String, ParamArray args())
End Sub
Public Sub reportSummary(ByVal count As Long, ByVal ignored As Long, ByVal success As Long)
End Sub
Public Sub onBeforeTest()
End Sub
Public Sub onTestEnd()
End Sub
Public Sub onClassTestsEnd()
End Sub
Public Sub onBeforeClassTests()
End Sub
Public Function theTest() As String

End Function

