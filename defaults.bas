Attribute VB_Name = "defaults"
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Public Function reportToDebug() As IReporter
    Set reportToDebug = New debugReporter
End Function

Public Function reportToSheet(ByRef reportingSheet As Worksheet) As IReporter
    Dim shtReporter As sheetReporter
    Set shtReporter = New sheetReporter
    Set shtReporter.reportingSheet = reportingSheet
    Set reportToSheet = shtReporter
End Function

Public Function reportToFile(ByVal fullPathName As String) As IReporter
    Dim oFileReporter As fileReporter
    Set oFileReporter = New fileReporter
    Call oFileReporter.init(fullPathName)
    Set reportToFile = oFileReporter
End Function
