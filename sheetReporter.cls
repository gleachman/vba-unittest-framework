VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "sheetReporter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Option Explicit
Implements IReporter

Private mSht As Worksheet
Private mTestName As String
Private mTestClass As String
Public Property Set reportingSheet(ByRef sheet As Worksheet)
    Set mSht = sheet
    mSht.Cells.Clear
    mSht.Cells(1, 1).Resize(1, 4).Value2 = Array("Class", "Test", "Pass/Fail", "Message")
End Property

Private Sub IReporter_onBeforeClassTests()
    
End Sub

Private Sub IReporter_onBeforeTest()

End Sub

Private Sub IReporter_onClassTestsEnd()

End Sub

Private Sub IReporter_onTestEnd()

End Sub

Private Sub IReporter_report(ByVal hasPassed As Boolean, ByVal msg As String, ParamArray args() As Variant)
    Dim v As Variant
    
    If hasPassed Then
        v = Array(mTestClass, mTestName, "PASSED", "")
    Else
        v = Array(mTestClass, mTestName, "FAILED", msg)
    End If
    
    Dim l As Long
    l = mSht.Cells(1, 1).CurrentRegion.Rows.count
    mSht.Cells(1, 1).Offset(l).Resize(1, 4) = v
    
    
End Sub

Private Sub IReporter_reportSummary(ByVal count As Long, ByVal ignored As Long, ByVal success As Long)
    Dim r As Range
    Dim v As Variant
    Dim l As Long
    l = mSht.Cells(1, 1).CurrentRegion.Rows.count
    v = Array("Tests Run", "Tests Successful", "Tests Ignored")
    mSht.Cells(1, 1).Offset(l).Resize(3, 1).Value2 = WorksheetFunction.Transpose(v)
    v = Array(count, success, ignored)
    mSht.Cells(1, 1).Offset(l, 1).Resize(3, 1).Value2 = WorksheetFunction.Transpose(v)
End Sub

Private Sub IReporter_setClass(ByVal className As String)
    mTestClass = className
End Sub

Private Sub IReporter_setTest(ByVal testName As String)
    mTestName = testName
End Sub

Private Function IReporter_theTest() As String

End Function
