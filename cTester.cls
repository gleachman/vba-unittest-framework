VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cTester"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Option Explicit

Implements ITestRunner
Private m_Reporter As IReporter
Attribute m_Reporter.VB_VarHelpID = -1
Private m_count As Long
Private m_success As Long
Private m_ignored As Long
Private m_tests As Long
Private m_testable As ITestAble

Private Function ITestRunner_AssertArrayEqual(ByVal lIs As Variant, ByVal lshouldbe As Variant, Optional msg As String) As Variant
    
    Dim counter As Long
    
    If UBound(lIs) <> UBound(lshouldbe) And LBound(lIs) <> LBound(lshouldbe) Then
        Call report(False, "Arrays are not the same size " & msg)
        Exit Function
    End If
    
    For counter = LBound(lIs) To UBound(lIs)
        If lIs(counter) <> lshouldbe(counter) Then
            Call report(False, " element (" & counter & ") doesn't match: " & msg)
            Exit Function
        End If
    Next counter
    
    Call report(True, msg)
    
End Function

Private Function ITestRunner_AssertEqual(ByVal lIs As Variant, ByVal lshouldbe As Variant, Optional msg As String) As Variant
        
    Call report(lIs = lshouldbe, msg)
    
End Function

Private Function ITestRunner_AssertTrue(ByVal val As Boolean, Optional ByVal msg As String) As Variant
    Call report(val, msg)
    
End Function

Private Function report(ByVal val As Boolean, msg)
    
    m_tests = m_tests + 1
    m_success = m_success + IIf(val, 1, 0)
    
    Call m_Reporter.report(val, msg)
    
End Function

Private Sub ITestRunner_run()
    Dim abk As Workbook
    For Each abk In Application.Workbooks
        If abk.Name <> ThisWorkbook.Name Then
            abk.VBProject.VBComponents.Item(1).CodeModule.CodePane.Show
            Call ITestRunner_runTestOnBook(abk.Name)
        End If
    Next abk
    
End Sub

Private Sub ITestRunner_runTest(testClassCaller As Object, ByVal testMethod As String)
    Dim testable As ITestAble
    Set testable = testClassCaller
    
    If Left(testMethod, 2) = "XX" Then
        'do not test this method
        m_ignored = m_ignored + 1
        Exit Sub
    End If
    
    Call m_Reporter.setTest(testMethod)
    testable.setUp
    m_Reporter.onBeforeTest
    
    Call CallByName(testClassCaller, testMethod, VbMethod)
    m_Reporter.onTestEnd
    testable.tearDown
    
End Sub



Private Sub ITestRunner_runTestOnBook(ByVal bookname As String)

    Dim abook As Workbook
    Set abook = Workbooks(bookname)
    Dim reflector As New cTestReflector
    Dim clsName As Variant
    Dim vProcs As Variant
    Dim vProc As Variant
    Dim testable As ITestAble
    Dim testObject As Object
    Dim amod As CodeModule
        
    Call reflector.extractTestMethodsFromTestBook(abook)
    
    Call reflector.createTestsClassGetters
    
    For Each clsName In reflector.testableClasses
    
        vProcs = reflector.dicOfTestClasses(clsName)
        
        Set testObject = Application.run(bookname & "!unittestFactory", clsName)
        Set testable = testObject
        
        Call testable.setTestRunner(Me)
        Call m_Reporter.setClass(clsName)
        
        Call m_Reporter.onBeforeClassTests
        Call ITestRunner_runTests(testObject, vProcs)
        Call m_Reporter.onClassTestsEnd
        
    Next clsName
    
    reflector.removeTestsClassGetters
    
    Call m_Reporter.reportSummary(m_tests, m_ignored, m_success)
    
    
End Sub


Private Sub ITestRunner_runTests(testClassCaller As Object, ByVal arrayOfTestNames As Variant)
    
    Dim test As Variant
    Dim testable As ITestAble
    Set testable = testClassCaller
    
    For Each test In arrayOfTestNames
        Call ITestRunner_runTest(testClassCaller, test)
    Next test
    
End Sub



Private Sub ITestRunner_setReporter(aReporter As IReporter)
    Set m_Reporter = aReporter
End Sub


