VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "debugReporter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Copyright 2014 Gareth Leachman
'VBA UnitTest Framework
'https://bitbucket.org/gleachman/vba-unittest-framework

'Licensed under the Apache License, Version 2.0 (the "License");
'you may not use this file except in compliance with the License.
'You may obtain a copy of the License at

'http://www.apache.org/licenses/LICENSE-2.0

'Unless required by applicable law or agreed to in writing, software
'distributed under the License is distributed on an "AS IS" BASIS,
'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'See the License for the specific language governing permissions and
'limitations under the License.Option Private Module

Implements IReporter

Private m_className As String
Private m_methodName As String

Private reporter As IReporter

Private Sub Class_Initialize()
    Debug.Print vbCr & Format(Now(), "dd-mmm-yyyy hh:mm:ss")
End Sub
Private Sub IReporter_endTests()
    Debug.Print vbCr
End Sub

Private Sub IReporter_onBeforeClassTests()
End Sub

Private Sub IReporter_onBeforeTest()
End Sub

Private Sub IReporter_onClassTestsEnd()
    Debug.Print vbCr
End Sub

Private Sub IReporter_onTestEnd()
End Sub

Private Sub IReporter_report(ByVal hasPassed As Boolean, ByVal msg As String, ParamArray args())
    If hasPassed Then
        Debug.Print "."
    Else
        Debug.Print vbCr & "!!!" & m_className & "." & m_methodName & " : " & msg
    End If
End Sub

Private Sub IReporter_reportSummary(ByVal count As Long, ByVal ignored As Long, ByVal success As Long)
    Debug.Print "Total: " & count & vbCr & "Failed: " & count - success & vbCr & "Ignored: " & ignored
End Sub

Private Sub IReporter_setClass(ByVal className As String)
    Debug.Print className
    m_className = className
End Sub

Private Sub IReporter_setTest(ByVal testName As String)
    m_methodName = testName
End Sub

Private Function IReporter_theTest() As String
    IReporter_theTest = m_methodName
End Function
